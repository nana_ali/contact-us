const http = require('http');
var swig  = require('swig'); 
var express = require('express');  
var app = express();  
const port = 3000;

app.get('/', function(req, res){  
	var template = swig.compileFile(__dirname + '/public/index.html');  
	var output = template({});  
	res.send(output); 
});  
app.listen(port);  	  
app.use(express.static('public')); 
